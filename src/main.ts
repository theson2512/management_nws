import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

const dotenv = require('dotenv')

dotenv.config();

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors({ origin: '*' });
  app.useGlobalPipes(new ValidationPipe());
  // app.setGlobalPrefix('api');

  const config = new DocumentBuilder()
    .setTitle('Management')
    .setDescription('API')
    .setVersion('1.0')
    .addTag('management')
    .addBearerAuth()
    .build();

  const document = SwaggerModule.createDocument(app, config)
  SwaggerModule.setup('/', app, document)

  await app.listen(process.env.PORT || 4001);
}
bootstrap();
