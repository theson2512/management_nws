import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsPhoneNumber } from 'class-validator';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { DepartmentEntity } from '../department/department.entity';

@Entity()
export class EmployeeEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @IsNotEmpty()
  @ApiProperty()
  nameEmployee: string;

  @Column()
  @ApiProperty()
  photo: string;

  @Column()
  @ApiProperty()
  jobTitle: string;

  @Column()
  @IsPhoneNumber()
  @ApiProperty()
  cellPhone: string;

  @Column()
  @IsEmail()
  @ApiProperty()
  email: string;

  @ManyToOne(() => DepartmentEntity, (department) => department.Manager)
  @ApiProperty()
  manager: string;
}
