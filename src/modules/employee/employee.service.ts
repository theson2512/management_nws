import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  IPaginationOptions,
  paginate,
  Pagination,
} from 'nestjs-typeorm-paginate';
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Like, Repository } from 'typeorm';
import {
  ICreateEmployee,
  IDeleteEmployee,
  IEmployee,
  IUpdateEmployee,
  MessageEmployee,
} from './dto/employee.dto';
import { EmployeeEntity } from './employee.entity';

@Injectable()
export class EmployeeService {
  constructor(
    @InjectRepository(EmployeeEntity)
    private readonly employeeRepo: Repository<EmployeeEntity>,
  ) {}

  paginate(options: IPaginationOptions): Observable<Pagination<IEmployee>> {
    const queryBuilder = this.employeeRepo
      .createQueryBuilder('a')
      .leftJoinAndSelect('a.manager', 'department_entity');
    return from(paginate<IEmployee>(queryBuilder, options)).pipe(
      map((usersPageable: Pagination<IEmployee>) => {
        usersPageable.items.forEach(function (v) {});
        return usersPageable;
      }),
    );
  }

  paginateFilter(
    options: IPaginationOptions,
    employee: IEmployee,
  ): Observable<Pagination<IEmployee>> {
    return from(
      this.employeeRepo.findAndCount({
        skip: (Number(options.page) - 1) * Number(options.limit) || 0,
        take: Number(options.limit) || 10,
        order: { id: 'ASC' },
        select: [
          'id',
          'nameEmployee',
          'photo',
          'jobTitle',
          'cellPhone',
          'email',
          'manager',
        ],
        where: [
          {
            nameEmployee: Like(`%${employee.nameEmployee}%`),
          },
        ],
        relations: ['manager'],
      }),
    ).pipe(
      map(([employees, totalEmployee]) => {
        const employeePaginate: Pagination<IEmployee> = {
          items: employees,
          meta: {
            currentPage: Number(options.page),
            itemCount: employees.length,
            itemsPerPage: Number(options.limit),
            totalItems: totalEmployee,
            totalPages: Math.ceil(totalEmployee / Number(options.limit)),
          },
        };
        return employeePaginate;
      }),
    );
  }

  async findAll(): Promise<EmployeeEntity[]> {
    try {
      return await this.employeeRepo.find({
        relations: ['manager'],
      });
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }
  async findEmployeeInDepartment(managerId: string): Promise<EmployeeEntity[]> {
    try {
      return await this.employeeRepo.find({
        relations: ['manager'],
        where: [
          {
            manager: managerId,
          },
        ],
      });
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }
  async findById(id: number): Promise<EmployeeEntity> {
    try {
      return await this.employeeRepo.findOne(id, { relations: ['manager'] });
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }
  async create(employee: IEmployee, file: string): Promise<ICreateEmployee> {
    try {
      const checkEmployee = await this.employeeRepo.findOne({
        nameEmployee: employee.nameEmployee,
      });
      if (checkEmployee) {
        return {
          statusCode: HttpStatus.BAD_REQUEST,
          message: MessageEmployee.exist,
        };
      }
      const newEmployee = new EmployeeEntity();
      newEmployee.nameEmployee = employee.nameEmployee;
      newEmployee.photo = file;
      newEmployee.jobTitle = employee.jobTitle;
      newEmployee.cellPhone = employee.cellPhone;
      newEmployee.email = employee.email;
      newEmployee.manager = employee.managerId;
      const dataSave = await this.employeeRepo.save(newEmployee);
      return {
        statusCode: HttpStatus.CREATED,
        message: MessageEmployee.add,
        dataSave,
      };
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }

  async update(employee: IEmployee, file: string, params: any): Promise<IUpdateEmployee> {
    try {
      const newEmployee = new EmployeeEntity();
      newEmployee.id = params.id;
      newEmployee.nameEmployee = employee.nameEmployee;
      newEmployee.photo = file;
      newEmployee.jobTitle = employee.jobTitle;
      newEmployee.cellPhone = employee.cellPhone;
      newEmployee.email = employee.email;
      newEmployee.manager = employee.managerId;
      const dataChange = await this.employeeRepo.update(
        newEmployee.id,
        newEmployee,
      );
      return { statusCode: HttpStatus.OK, message: MessageEmployee.update, dataChange };
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }
  async delete(id): Promise<IDeleteEmployee> {
    try {
      const checkEmployee = await this.employeeRepo.findOne({ id: id });
      if (!checkEmployee) {
        return {
          statusCode: HttpStatus.NOT_FOUND,
          message: MessageEmployee.doNotExist,
        };
      }
      await this.employeeRepo.delete(id);
      return { statusCode: HttpStatus.OK, message: MessageEmployee.delete };
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }
}
