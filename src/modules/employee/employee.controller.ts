import {
  Body,
  Controller,
  DefaultValuePipe,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  Res,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiConsumes,
  ApiForbiddenResponse,
  ApiOkResponse,
  ApiParam,
  ApiQuery,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { Pagination } from 'nestjs-typeorm-paginate';
import { Observable } from 'rxjs';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { ApiFile } from '../multer/ApiFile';
import { multerOptions } from '../multer/multerOptions';
import { RolesGuard } from '../role/roles.guard';
import { IEmployee } from './dto/employee.dto';
import { EmployeeEntity } from './employee.entity';
import { EmployeeService } from './employee.service';

@Controller('employee')
export class EmployeeController {
  constructor(private readonly employeeService: EmployeeService) {}

  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiQuery({ name: 'nameEmployee', required: false })
  @ApiOkResponse({ description: 'OK' })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @Get('paginate')
  filterEmployee(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number = 1,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number = 10,
    @Query('nameEmployee') nameEmployee: string,
  ): Observable<Pagination<IEmployee>> {
    limit = limit > 100 ? 100 : limit;
    if (nameEmployee === null || nameEmployee === undefined) {
      return this.employeeService.paginate({
        page: Number(page),
        limit: Number(limit)
      });
    } else {
      return this.employeeService.paginateFilter(
        {
          page: Number(page),
          limit: Number(limit)
        },
        { nameEmployee },
      );
    }
  }

  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'OK' })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @Get()
  findAll(): Promise<EmployeeEntity[]> {
    return this.employeeService.findAll();
  }

  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiParam({ name: 'id' })
  @ApiOkResponse({ description: 'OK' })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @Get(':id')
  findById(@Param() params) {
    return this.employeeService.findById(params);
  }

  // @UseGuards(JwtAuthGuard)
  // @ApiBearerAuth()
  @Get('uploads/:photo')
  serverImage(@Param('photo') photo, @Res() res): Promise<any> {
    return res.sendFile(photo, { root: 'uploads' });
  }

  @UseGuards(JwtAuthGuard, RolesGuard)
  @ApiBearerAuth()
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @ApiOkResponse({ description: 'OK' })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiForbiddenResponse({ description: 'Forbidden' })
  @UseInterceptors(FileInterceptor('photo', multerOptions))
  @Post()
  create(@Body() body, @UploadedFile() file: Express.Multer.File) {
    return this.employeeService.create(body, file.path);
  }

  @UseGuards(JwtAuthGuard, RolesGuard)
  @ApiBearerAuth()
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @ApiParam({ name: 'id' })
  @ApiOkResponse({ description: 'OK' })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiForbiddenResponse({ description: 'Forbidden' })
  @UseInterceptors(FileInterceptor('photo', multerOptions))
  @Put(':id')
  update(
    @Body() employee: IEmployee,
    @UploadedFile() file: Express.Multer.File,
    @Param() params,
  ) {
    return this.employeeService.update(employee, file.path, params);
  }

  @UseGuards(JwtAuthGuard, RolesGuard)
  @ApiBearerAuth()
  @ApiParam({ name: 'id' })
  @ApiOkResponse({ description: 'OK' })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiForbiddenResponse({ description: 'Forbidden' })
  @Delete(':id')
  delete(@Param() params) {
    return this.employeeService.delete(params.id);
  }
}
