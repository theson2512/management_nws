export interface IEmployee {
  id?: number;
  nameEmployee?: string;
  photo?: string;
  jobTitle?: string;
  cellPhone?: string;
  email?: string;
  managerId?: string;
}

export enum MessageEmployee {
  exist = 'Nhân viên đã tồn tại trong hệ thống !',
  doNotExist = 'Nhân viên không tồn tại trong hệ thống !',
  add = 'Thêm thành công !',
  update = 'Sửa thành công !',
  delete = 'Xóa thành công !',
}

export interface ICreateEmployee {
  statusCode: number;
  message: string;
  dataSave?: IEmployee;
}
export interface IUpdateEmployee {
  statusCode: number;
  message: string;
  dataChange?: object;
}
export interface IDeleteEmployee {
  statusCode: number;
  message: string;
}
