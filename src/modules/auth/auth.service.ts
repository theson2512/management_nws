import { Body, HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { UserService } from 'src/modules/user/user.service';
import { Repository } from 'typeorm';
import { UserEntity } from '../user/user.entity';
import {
  ISaveDataUser,
  IUserLogin,
  MessageRegisterByMail,
  RegisterByMail,
} from './dto/auth.dto';

const bcrypt = require('bcrypt');
let nodemailer = require('nodemailer');
let password = Math.random().toString(36).substr(2, 8);
@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepo: Repository<UserEntity>,
    private userService: UserService,
    private jwtService: JwtService,
  ) {}

  async validateUser(username: string, password: string): Promise<any> {
    const user = await this.userService.findUser(username);
    if (!user) return { statusCode: 404, message: 'Không tìm thấy tài khoản' };
    const match = await bcrypt.compare(password, user.password);
    if (user && match) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }

  async login(user: IUserLogin) {
    try {
      if (!user.statusCode) {
        const payload = {
          username: user.username,
          email: user.email,
          role: user.role,
          loginFirst: user.loginFirst,
        };
        return {
          accessToken: this.jwtService.sign(payload, { expiresIn: 60 * 60 }),
          expiresIn: 60 * 60,
        };
      }
      return user;
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }

  async saveDataUser(data: ISaveDataUser): Promise<UserEntity> {
    const newUserName = data.username.replace(/ /g, '');
    const dataNew = {
      username: (data.username ? newUserName : data.email).toLowerCase(),
      password: password,
      email: data.email,
    };
    return await this.userService.saveUserRegisterByMail(dataNew);
  }

  async sendEmail(@Body() data): Promise<RegisterByMail> {
    try {
      const checkEmployee = await this.userRepo.findOne({
        username: data.username,
      });
      if (checkEmployee) {
        return {
          statusCode: HttpStatus.BAD_REQUEST,
          message: MessageRegisterByMail.accountUsed,
        };
      }

      const checkEmail = await this.userRepo.findOne({ email: data.email });
      if (checkEmail) {
        return {
          statusCode: HttpStatus.BAD_REQUEST,
          message: MessageRegisterByMail.emailUsed,
        };
      }

      const output = `
            <p>Tài khoản của bạn đã được tạo tại Newwave Solution JSC Management</p>
            <h3>Thông tin chi tiết tài khoản</h3>
            <ul>
                <li>Tên đăng nhập : ${(data.username
                  ? data.username
                  : data.email
                ).toLowerCase()}</li>
                <li>Mật khẩu : ${password}</li>
            </ul>
            <p>Vui lòng đổi mật khẩu sau khi nhận được mail này !</p>
            `;
      await nodemailer.createTestAccount();
      let transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 587,
        ignoreTLS: false,
        secure: false,
        auth: {
          user: `${process.env.user}`,
          pass: `${process.env.pass}`,
        },
      });

      await transporter.sendMail({
        from: `" Newwave Solution JSC " <${data.emailAdmin}>`,
        to: `${data.email}`,
        subject: 'Thông tin tài khoản ✔',
        text: 'Chúc mừng bạn đã tạo tài khoản thành công !',
        html: output,
      });
      await this.saveDataUser(data);
      return {
        statusCode: HttpStatus.OK,
        message: MessageRegisterByMail.registerByMailSuccessful,
      };
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }
}
