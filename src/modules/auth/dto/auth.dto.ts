import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, MinLength } from 'class-validator';

export interface IUserLogin {
  id: number;
  username: string;
  email: string;
  role: number;
  loginFirst: boolean;
  statusCode?: number;
  message?: string;
}

export interface ISaveDataUser {
  username: string;
  password?: string;
  email: string;
}

export class RegisterDto {
  @MinLength(3)
  @ApiProperty()
  username: string;

  @IsEmail()
  @IsNotEmpty()
  @ApiProperty()
  email: string;

  @IsNotEmpty()
  @ApiProperty()
  emailAdmin: string;
}

export class LoginDto {
  @IsNotEmpty()
  @ApiProperty()
  username: string;

  @IsNotEmpty()
  @ApiProperty({ description: 'Lớn hơn 6 ký tự' })
  @MinLength(6)
  password: string;
}

export enum MessageRegisterByMail {
  accountUsed = 'Tên tài khoản đã được sử dụng !',
  emailUsed = 'Email đã được sử dụng !',
  registerByMailSuccessful = 'Gửi email thành công !',
}

export interface RegisterByMail {
  statusCode: number;
  message: string;
}
