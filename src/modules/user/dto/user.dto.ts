import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, MinLength } from 'class-validator';

export class ChangePasswordDto {
  @IsNotEmpty()
  @ApiProperty()
  username: string;

  @IsNotEmpty()
  @ApiProperty({ description: 'Lớn hơn 6 ký tự' })
  @MinLength(6)
  oldPass: string;

  @IsNotEmpty()
  @ApiProperty({ description: 'Lớn hơn 6 ký tự' })
  @MinLength(6)
  newPass: string;

  @IsNotEmpty()
  @ApiProperty({ description: 'Lớn hơn 6 ký tự' })
  @MinLength(6)
  confirm: string;
}

export enum MessageUser {
  exist = 'Người dùng đã tồn tại trong hệ thống !',
  update = 'Sửa thành công !',
  delete = 'Xóa thành công !',
  accountDoNotExist = 'Tài khoản không tồn tại',
  passOldWrong = 'Mật khẩu cũ chưa chính xác',
  comparePassOldAndPassNew = 'Mật khẩu mới phải khác mật khẩu cũ',
  comparePassNewAndPassConfirm = 'Mật khẩu xác nhận chưa trùng khớp',
  changePassSuccess = 'Thay đổi mật khẩu thành công',
}

export interface IUpdateUser {
  statusCode: number;
  message: string;
}
export interface IDeleteUser {
  statusCode: number;
  message: string;
}
export interface IChangePass {
  statusCode: number;
  message: string;
}
