import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  IPaginationOptions,
  paginate,
  Pagination,
} from 'nestjs-typeorm-paginate';
import { Repository } from 'typeorm';
import { ISaveDataUser } from '../auth/dto/auth.dto';
import {
  IChangePass,
  IDeleteUser,
  IUpdateUser,
  MessageUser,
} from './dto/user.dto';
import { UserEntity } from './user.entity';

const bcrypt = require('bcrypt');
const saltRounds = 10;
@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepo: Repository<UserEntity>,
  ) {}

  async paginate(options: IPaginationOptions): Promise<Pagination<UserEntity>> {
    return paginate<UserEntity>(this.userRepo, options);
  }

  async findAll(): Promise<UserEntity[]> {
    try {
      const data = await this.userRepo.find();
      return data;
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }
  async findById(id: number): Promise<UserEntity> {
    try {
      const user = await this.userRepo.findOne(id);
      return user;
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }

  async saveUserRegisterByMail(user: ISaveDataUser): Promise<any> {
    try {
      const checkEmployee = await this.userRepo.findOne({
        username: user.username,
      });
      if (checkEmployee) {
        return {
          statusCode: HttpStatus.BAD_REQUEST,
          message: MessageUser.exist,
        };
      }

      const resultAdmin = new UserEntity();
      resultAdmin.username = user.username;
      resultAdmin.password = user.password;
      resultAdmin.email = user.email;

      return await this.userRepo.save(resultAdmin);
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }
  async update(user: UserEntity, params: any): Promise<IUpdateUser> {
    try {
      const resultAdmin = new UserEntity();
      resultAdmin.id = params.id;
      resultAdmin.username = user.username;
      resultAdmin.email = user.email;
      resultAdmin.role = user.role;
      await this.userRepo.update(resultAdmin.id, resultAdmin);
      return { statusCode: HttpStatus.OK, message: MessageUser.update };
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }
  async delete(id): Promise<IDeleteUser> {
    try {
      await this.userRepo.delete(id);
      return { statusCode: HttpStatus.OK, message: MessageUser.delete };
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }
  async findUser(username: string): Promise<UserEntity> {
    return await this.userRepo.findOne({ username: username });
  }
  async changePass(
    username: string,
    oldPass: string,
    newPass: string,
    confirm: string,
  ): Promise<IChangePass> {
    try {
      const user = await this.findUser(username);

      if (!user) {
        return {
          statusCode: HttpStatus.NOT_FOUND,
          message: MessageUser.accountDoNotExist,
        };
      }

      const match = await bcrypt.compare(oldPass, user.password);
      const checkPass = newPass === confirm;
      const checkNewAndOldPass = oldPass !== newPass;

      if (!match) {
        return {
          statusCode: HttpStatus.BAD_REQUEST,
          message: MessageUser.passOldWrong,
        };
      }

      if (!checkNewAndOldPass) {
        return {
          statusCode: HttpStatus.BAD_REQUEST,
          message: MessageUser.comparePassOldAndPassNew,
        };
      }

      if (!checkPass) {
        return {
          statusCode: HttpStatus.BAD_REQUEST,
          message: MessageUser.comparePassNewAndPassConfirm,
        };
      }

      if (user && match && checkPass) {
        newPass = await bcrypt.hash(newPass, saltRounds );
        await this.userRepo.query(
          `UPDATE user_entity SET password = '${newPass}', "loginFirst" = true WHERE id = ${user.id}`,
        );
        return {
          statusCode: HttpStatus.OK,
          message: MessageUser.changePassSuccess,
        };
      }
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }
}
