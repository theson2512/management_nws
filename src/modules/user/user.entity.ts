import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty } from 'class-validator';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  BeforeInsert,
  BeforeUpdate,
} from 'typeorm';

const bcrypt = require('bcrypt');
const saltRounds = 10;

@Entity()
export class UserEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @IsNotEmpty()
  @ApiProperty({ type: String })
  username: string;

  @Column()
  password: string;

  @Column()
  @IsEmail()
  @ApiProperty()
  email: string;

  @Column({ default: 0 })
  @ApiProperty({ required: true, default: 0 })
  role: number;

  @Column({ default: false })
  loginFirst: boolean;

  @BeforeInsert()
  async hashPassword() {
    this.password = await bcrypt.hash(this.password, saltRounds);
  }

  // @BeforeUpdate()
  // async hashPasswordUpdate() {
  //     this.password = await bcrypt.hash(this.password, saltRounds)
  // }
}
