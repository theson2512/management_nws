import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  IPaginationOptions,
  paginate,
  Pagination,
} from 'nestjs-typeorm-paginate';
import { Repository } from 'typeorm';
import { EmployeeService } from '../employee/employee.service';
import { DepartmentEntity } from './department.entity';
import {
  ICreateDepartment,
  IDeleteDepartment,
  IDepartment,
  IUpdateDepartment,
  MessageDepartment,
} from './dto/department.dto';

@Injectable()
export class DepartmentService {
  constructor(
    @InjectRepository(DepartmentEntity)
    private readonly departmentRepo: Repository<DepartmentEntity>,
    private readonly employeeService: EmployeeService,
  ) {}

  async paginate(
    options: IPaginationOptions,
  ): Promise<Pagination<DepartmentEntity>> {
    return paginate<DepartmentEntity>(this.departmentRepo, options);
  }

  async findAll(): Promise<DepartmentEntity[]> {
    try {
      return await this.departmentRepo.find();
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }

  async findById(id: number): Promise<IDepartment> {
    try {
      return await this.departmentRepo.findOne(id);
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }

  async findEmployee(managerId: string): Promise<any> {
    try {
      return await this.employeeService.findEmployeeInDepartment(managerId);
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }

  async create(department: DepartmentEntity): Promise<ICreateDepartment> {
    try {
      const checkDepartment = await this.departmentRepo.findOne({
        nameDepartment: department.nameDepartment,
      });
      if (checkDepartment) {
        return {
          statusCode: HttpStatus.BAD_REQUEST,
          message: MessageDepartment.exist,
        };
      }
      const dataSave = await this.departmentRepo.save(department);
      return {
        statusCode: HttpStatus.CREATED,
        message: MessageDepartment.add,
        dataSave,
      };
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }
  async update(
    department: DepartmentEntity,
    params: any,
  ): Promise<IUpdateDepartment> {
    try {
      const newDepartment = new DepartmentEntity();
      newDepartment.id = params.id;
      newDepartment.nameDepartment = department.nameDepartment;
      newDepartment.officePhone = department.officePhone;

      const dataChange = await this.departmentRepo.update(
        newDepartment.id,
        newDepartment,
      );
      return {
        statusCode: HttpStatus.OK,
        message: MessageDepartment.update,
        dataChange,
      };
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }
  async delete(id): Promise<IDeleteDepartment> {
    try {
      const checkDepartment = await this.departmentRepo.findOne({ id: id });
      if (!checkDepartment) {
        return {
          statusCode: HttpStatus.NOT_FOUND,
          message: MessageDepartment.doNotExist,
        };
      }
      await this.departmentRepo.delete(id);
      return { statusCode: HttpStatus.OK, message: MessageDepartment.delete };
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }
}
