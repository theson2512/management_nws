export interface IDepartment {
  id: number;
  nameDepartment: string;
  officePhone: string;
}

export enum MessageDepartment {
  exist = 'Văn phòng đã tồn tại !',
  doNotExist = 'Văn phòng không tồn tại !',
  add = 'Thêm thành công !',
  update = 'Sửa thành công !',
  delete = 'Xóa thành công !'
}

export interface ICreateDepartment {
  statusCode: number;
  message: string;
  dataSave?: IDepartment;
}
export interface IUpdateDepartment {
  statusCode: number;
  message: string;
  dataChange?: object;
}
export interface IDeleteDepartment {
  statusCode: number;
  message: string;
}
