import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsPhoneNumber } from 'class-validator';
import {
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { EmployeeEntity } from '../employee/employee.entity';

@Entity()
export class DepartmentEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @IsNotEmpty()
  @ApiProperty()
  nameDepartment: string;

  @Column()
  @IsNotEmpty()
  @IsPhoneNumber()
  @ApiProperty()
  officePhone: string;

  @OneToMany(() => EmployeeEntity, (employee) => employee.manager)
  @JoinColumn()
  Manager: Promise<EmployeeEntity[]>;
}
